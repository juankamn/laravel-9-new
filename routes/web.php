<?php

//use App\Http\Controllers\PostController;
use App\Enums\PostState;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
//    return str('hello world')->upper();
//    return str('hello world')->slug();
//    return str()->slug('hello world');
//    throw new \Exception('Whooooops');
//    return Blade::render('{{ $greeting }}, World', ['greeting' => 'Hello']);

//    return view('welcome');

    $post = new Post;

    $post->state = PostState::Published;


//    return Post::first()->state->value;

//    return Post::search('quisquam')->get();

});

Route::get('/post/{post}', function (Post $post){
    return $post;
})->name('posts.show');

//Route::get('/users/{user}/post/{post:id}', function (User $user, Post $post){
//    return $post;
//});

Route::get('/post/{state}', function (PostState $state){
    dd($state);
});

Route::get('/users/{user}/post/{post}', function (User $user, Post $post){
    return $post;
})->scopeBindings();

//Route::get('/endpoint', function (){
//   return redirect('/');
//    return redirect()->route('home');
//    return to_route('home'); // === return redirect()->route('home');
//});

//Route::controller(PostController::class)->group(function () {
//    Route::get('/posts', 'index');
//    Route::get('/posts/{post}', 'show');
//    Route::post('/posts', 'store');
//});
